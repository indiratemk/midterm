@extends('layout')

@section('content')

    <div class="container">
        <h3>Questions</h3>
        <br>
        <div class="row">
            {!! Form::open(['route' => 'question.update']) !!}
            <div class="form-group">
                <input type="text" class="form-control" name="name">
                <br>
                <textarea name="question" id="" cols="30" rows="10" class="form-control">
                </textarea>
                <br>
                <button class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection