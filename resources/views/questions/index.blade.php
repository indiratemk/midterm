@extends('layout')

@section('content')

    <div class="container">
        <h3>Questions</h3>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <table class="table">
                <thead>
                <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Question</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                @foreach($quests as $quest)
                <td>{{ $quest->id }}</td>
                <td>{{ $quest->name }}</td>
                <td>{{ $quest->question }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
            {{ $quests->links() }}
    </div>
        <a href="/question/edit" class="btn btn-success">New question</a>
    </div>
@endsection