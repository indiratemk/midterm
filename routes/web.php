<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/questions', 'QuestionController@index')->name('questions.index');
Route::get('/question/edit', 'QuestionController@edit')->name('question.edit');
Route::post('/question/update', 'QuestionController@update')->name('question.update');
