<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class QuestionController extends Controller
{
    public function index(){
        $quests = DB::table('questions')->paginate(3);
        return view('questions.index', ['quests' => $quests]);
    }
    public function edit(){
        return view('questions.edit');
    }
    public function update(Request $request){
        DB::table('questions')->insert(
            ['name' => $request->name, 'question' => $request->question]
        );
        return redirect()->route('questions.index');
    }
}
